from django.db import models
from django.utils.translation import ugettext_lazy


class Tweet(models.Model):

    username = models.CharField(
        verbose_name=ugettext_lazy("Posters username"),
        max_length=50,
        blank=False,
        null=False
    )

    body = models.CharField(
        verbose_name=ugettext_lazy("Post body"),
        max_length=50,
        blank=False,
        null=False
    )

    date_added = models.DateTimeField(
        verbose_name=ugettext_lazy("Date added"),
        auto_now_add=True,
        blank=False,
        null=False,
    )