# coding=utf-8
from rest_framework import serializers

from . import models


class PostSerializer(serializers.ModelSerializer):

    date_added = serializers.DateTimeField(read_only=True)

    class Meta:
        model = models.Tweet
        fields = [
            'id',
            'username',
            'body',
            'date_added'
        ]