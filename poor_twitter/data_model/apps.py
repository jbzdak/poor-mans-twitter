from django.apps import AppConfig


class DataModelConfig(AppConfig):
    name = 'poor_twitter.data_model'
