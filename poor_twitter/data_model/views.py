from rest_framework.filters import OrderingFilter
from rest_framework.viewsets import ModelViewSet

from . import models, serializers


class PostViewset(ModelViewSet):

    ordering_fields = ('username', 'date_added')
    serializer_class =  serializers.PostSerializer
    filter_backends = (OrderingFilter,)
    queryset = models.Tweet.objects.all()