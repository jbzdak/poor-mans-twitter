"""
poor_twitter URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from poor_twitter.data_model.views import PostViewset
from poor_twitter.feed.views import FeedView

router = routers.SimpleRouter()
router.register(r'posts', PostViewset)

api_urlpatterns = router.urls


urlpatterns = [
    path('', FeedView.as_view()),
    path('admin/', admin.site.urls),
    path('api/', include(api_urlpatterns))
]
