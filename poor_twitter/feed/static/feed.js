(function () {
    var vm = new Vue({
        el: "#feed",
        "data": function () {
            return {
                "data_loaded": false,
                "posts": [],
                'username': '',
                'body': '',
                'post_error': false,
                'sort': 'date'
            }
        },
        'watch': {
            'sort': function () {
                this.loadData();
            }
        },
        'computed': {
            'fetchUrl': function () {
                var params = '';
                if (this.sort === 'date') {
                    params = '?ordering=date_added';
                }
                if (this.sort === 'username') {
                    params = '?ordering=username'
                }
                return "/api/posts/" + params;
            },
            'postUrl': function () {
                return "/api/posts/";
            },
        },
        'methods':{
            'doPost': function () {
                var self = this;
                window.fetch(self.postUrl, {
                    'method': 'POST',
                    'body': JSON.stringify({
                        'username': this.username,
                        'body': this.body
                    }),
                    'headers':    {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    if (response.status !== 201){
                        self.onInvalidPostResponse();
                        return;
                    }
                    self.username = '';
                    self.body = '';
                    self.post_error = '';
                    self.loadData();
                }).catch(function () {
                    self.onInvalidPostResponse();
                })

            },
            'onInvalidPostResponse': function () {
                alert("Error posting, try again! ")
            },
            'onInvalidLoadResponse': function () {
                // Some fancy retry logic that depends on what requirements
                // so not implemented here ;)
                alert("Error loading data, please reload the page.")
            },
            'loadData': function () {
                var self = this;
                fetch(this.fetchUrl).then(function (response) {
                    if (!response.ok){
                        self.onInvalidLoadResponse();
                        return;
                    }
                    response.json().then(function (posts) {
                        self.posts = posts;
                        self.data_loaded = true;
                    });

                }).catch(function () {
                    self.onInvalidLoadResponse()
                })
            }
        },
        'created': function () {
            this.loadData();
        },
        'template': `
        <div class="container">
            <h1>Post something! </h1>
            <div class="form-group row">
                <label for="username" class="col-md-2 col-form-label">Username</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="username" v-model="username" maxlength="50">
                </div>
            </div>
            <div class="form-group row">
                <label for="body" class="col-md-2 col-form-label">Body</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="body" v-model="body" maxlength="50">
                </div>
            </div>
            <button
                    v-on:click="doPost"
                    class="btn btn-primary"
                    :disabled="body.length==0 || username.length==0">Submit post</button>        
            <h1 class="mt-3">Your feed</h1>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">Body</th>
                    <th scope="col" v-on:click="sort='username'">
                        User <span v-if="sort === 'username'">(sort column)</span></th>
                    <th scope="col" v-on:click="sort='date'">
                        Posted at <span v-if="sort === 'date'">(sort column)</span></th>
                </tr>
                </thead>
                <tr v-if="!data_loaded">
                    <td colspan="3">Please wait while we are loading data</td>
                </tr>
                <tr v-else-if="posts.length==0">
                    <td colspan="3">Nothing to show</td>
                </tr>
                <tbody v-else>
                <tr v-for="(item, index) in posts" :key="item.id">
                    <td>{{item.body}}</td>
                    <td>{{item.username}}</td>
                    <td>{{item.date_added}}</td>
                </tr>
                </tbody>
            </table>
        
        </div>   
        `
    })
})();